import * as React from 'react';
import {Link} from 'react-router-dom';
import logo from "./13_Bild_Suchspiel.png";
import wtf2s from "./Vector2.svg"
import wtf2p from "./wtf2.png"

// eslint-disable-next-line react/prefer-stateless-function
export class play extends React.Component {
    render(): JSX.Element {
        return (
            <div>
                <h2>
                    <img className="logo13" src={logo} alt=""/>
                </h2>
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="210mm"
                    height="297mm"
                    viewBox="0 0 210 297"
                    version="1.1"
                    id="svg8">
                    <defs
                        id="defs2">
                        <linearGradient
                            gradientUnits="userSpaceOnUse"
                            y2="328"
                            x2="260.25"
                            y1="1"
                            x1="260.25"
                            id="paint0_linear"/>
                    </defs>
                    <a href="/sdgFUCKOFF">
                        <g
                            style={{fill: "none"}}
                            id="g35"
                            transform="matrix(0.26458333,0,0,0.26458333,39.999331,54.688778)">
                            <path
                                id="path10"
                                d="M 29,120 H 0.5 V 65.5 61.5 59 l 116,49 2.5,-31.5 20,-29 L 251,108 246,85 274.5,26.5 330,96 352.5,55.5 377,70.5 452,1 484,47.5 520,127 v 49 l -32.5,36.5 -24,40 -69,5.5 -31.5,17 -48.5,10 -5,11.5 -35,31.5 L 91,285 67.5,249 38,201.5 47.5,171 72,160 55,135 Z"
                                style={{fill: "#c4c4c4"}}/>
                            <path
                                id="path12"
                                d="M 29,120 H 0.5 V 65.5 61.5 59 l 116,49 2.5,-31.5 20,-29 L 251,108 246,85 274.5,26.5 330,96 352.5,55.5 377,70.5 452,1 484,47.5 520,127 v 49 l -32.5,36.5 -24,40 -69,5.5 -31.5,17 -48.5,10 -5,11.5 -35,31.5 L 91,285 67.5,249 38,201.5 47.5,171 72,160 55,135 Z"
                                style={{fill: "url(#paint0_linear)"}}/>
                            <path
                                id="path14"
                                d="m 0.5,61.5 v 4 m 0,0 V 120 H 29 l 26,15 17,25 -24.5,11 -9.5,30.5 29.5,47.5 23.5,36 183.5,43 35,-31.5 5,-11.5 48.5,-10 31.5,-17 69,-5.5 24,-40 L 520,176 V 127 L 484,47.5 452,1 377,70.5 352.5,55.5 330,96 274.5,26.5 246,85 251,108 139,47.5 119,76.5 116.5,108 0.5,59 Z"
                                style={{stroke: "#000000"}}/>
                        </g>
                    </a>
                </svg>
            </div>
        );
    }
}

export default play
